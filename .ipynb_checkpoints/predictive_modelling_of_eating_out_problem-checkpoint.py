# -*- coding: utf-8 -*-
"""Predictive Modelling of Eating-Out Problem.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1ji4s3vBi0P_fCm7Y_4MmAhgIKAGarC-G
"""

#importing libraries
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

"""## Part A –Importing and Understanding Data
### Data Loading and Initial Exploration
 Loading the dataset and performing some initial exploration to understand its structure and contents.
"""

# Updated dataset URL
data_url_modified = "https://raw.githubusercontent.com/eliasedwin7/GPT-Prompts/main/zomato_df_final_data.csv"
data = pd.read_csv(data_url_modified)

import ast
# Convert the 'cuisine' column from string representation of list to actual list
data['cuisine'] = data['cuisine'].apply(ast.literal_eval)


# Display the first few rows of the dataset
data.head()

data.describe()

data.info()

"""### Exploratory Data Analysis (EDA)
1.How many unique cuisines are served by Sydney restaurants?
"""

# Extract all unique cuisines
all_cuisines = [cuisine for sublist in data['cuisine'].tolist() for cuisine in sublist]
unique_cuisines = set(all_cuisines)
# Number of unique cuisines
print(f"There are {len(unique_cuisines)} unique  cuisines served by Sydney restaurents")

"""2.Which suburbs (top-3) have the highest number of restaurants?"""

import matplotlib.pyplot as plt
import seaborn as sns
# Count the number of restaurants in each suburb
subzone_counts = data['subzone'].value_counts().head(3)
# Plot
plt.figure(figsize=(10, 6))
sns.barplot(x=subzone_counts.index, y=subzone_counts.values, palette='viridis')
plt.title('Top 3 Suburbs with the Highest Number of Restaurants')
plt.xlabel('Suburb')
plt.ylabel('Number of Restaurants')
plt.show()

"""The top 3 suburbs with the highest number of restaurants in Sydney are:

- CBD (Central Business District)
- Surry Hills
- Darlinghurst

3.“Restaurants with ‘excellent’ rating are mostly very expensive while those with
‘Poor’ rating are rarely expensive”. Do you agree on this statement or not? Please
support your answer by numbers and visuals. (hint: use stacked bar chart or
histogram to relate ‘cost’ to 'rating_text')
"""

# Group data by 'rating_text' and get the average cost
avg_cost_by_rating = data.groupby('rating_text')['cost'].mean()
# Plot
plt.figure(figsize=(12, 7))
sns.barplot(x=avg_cost_by_rating.index, y=avg_cost_by_rating.values, palette='viridis')
plt.title('Average Cost by Restaurant Rating')
plt.xlabel('Rating Text')
plt.ylabel('Average Cost (AUD)')
plt.show()

"""The bar chart above displays the average cost by restaurant rating:
- Restaurants with a "Poor" rating have a lower average cost compared to those with higher ratings.
- Restaurants with an "Excellent" rating have a higher average cost, but not the highest. The "Very Good" rating category has the highest average cost.
- The "Good" and "Average" rating categories have similar average costs.

From the chart, we can infer that while restaurants with an "Excellent" rating tend to be more expensive on average, they are not necessarily the most expensive. Similarly, restaurants with a "Poor" rating tend to be less expensive, but there are other categories with similar average costs.

Thus, the statement "Restaurants with ‘excellent’ rating are mostly very expensive while those with ‘Poor’ rating are rarely expensive" is partially accurate. While "Excellent" rated restaurants tend to be more expensive, they aren't the most expensive on average. "Poor" rated restaurants are among the least expensive, but not the absolute lowest.

### Exploratory analysis for the variables of the data
"""

# Distribution plot for 'Cost'
plt.figure(figsize=(12, 7))
sns.histplot(data['cost'], bins=50, kde=True, color='purple')
plt.title('Distribution of Restaurant Cost')
plt.xlabel('Cost (AUD)')
plt.ylabel('Number of Restaurants')
plt.show()

"""The distribution plot for the "Cost" variable shows:
- Most restaurants have an average cost for two people between approximately AUD 20 and AUD 100.
- There's a peak around AUD 50, indicating that many restaurants in Sydney have an average cost of around AUD 50 for two people.
- Few restaurants have an average cost exceeding AUD 200.
"""

# Distribution plot for 'rating_number'
plt.figure(figsize=(12, 7))
sns.histplot(data['rating_number'], bins=20, kde=True, color='blue')
plt.title('Distribution of Restaurant Ratings')
plt.xlabel('Rating')
plt.ylabel('Number of Restaurants')
plt.show()

"""The distribution plot for the "Rating" variable shows:

- Most restaurants have ratings between approximately 3.5 and 4.5.
- There's a significant peak around a rating of 4.0, indicating that many restaurants in Sydney have an average rating of 4.0.
- Few restaurants have ratings below 3.0 or above 4.7.

### Handling Missing Values
"""

#missing data
sns.heatmap(data.isnull(),yticklabels=False,cbar=False,cmap='viridis')

# Check for missing values in the dataset
missing_values = data.isnull().sum()
print("Missing Values Before Handling:", missing_values)

# Handle missing values
data['type'].fillna("['Unknown']", inplace=True)

# Convert the 'type' column to actual lists
data['type'] = data['type'].apply(ast.literal_eval)

data['rating_number'].fillna(data['rating_number'].mean(), inplace=True)
data['cost'].fillna(data['cost'].mean(), inplace=True)
data['rating_text'].fillna('Unknown', inplace=True)
# Drop rows where either 'lat' or 'lng' is missing
data.dropna(subset=['lat', 'lng'], inplace=True)

# Handle missing values for 'votes' by filling with zero
data['votes'].fillna(0, inplace=True)

# Handle missing values for 'cost_2' by filling with the mean
data['cost_2'].fillna(data['cost_2'].mean(), inplace=True)

# Verify if missing values are handled
missing_values_after = data.isnull().sum()
print("Missing Values After Handling:", missing_values_after)

# Flatten the list of types into a single list
all_types = [item for sublist in data['type'].tolist() for item in sublist]

# Create a DataFrame from the list
df_types = pd.DataFrame(all_types, columns=['Type'])

# Plot the distribution of the 'Type' variable after flattening
plt.figure(figsize=(15, 8))
ax = sns.countplot(data=df_types, y='Type', order=df_types['Type'].value_counts().index)
plt.title('Distribution of Restaurant Types')
plt.xlabel('Count')
plt.ylabel('Type')

# Add annotations to show the actual counts
for p in ax.patches:
    ax.annotate(f'{int(p.get_width())}', (p.get_width(), p.get_y() + p.get_height()/2),
                ha='left', va='center')

plt.show()

import geopandas as gpd
import matplotlib.pyplot as plt


#geojson_url = 'https://raw.githubusercontent.com/eliasedwin7/GPT-Prompts/2b6aca7b43b22d29acae46e8a6e0543c7e8d1b6b/sydney.geojson'
#response = requests.get(geojson_url)
#geojson_data = response.json()
# Load the Sydney geojson file
sydney_gdf = gpd.read_file('https://raw.githubusercontent.com/eliasedwin7/GPT-Prompts/2b6aca7b43b22d29acae46e8a6e0543c7e8d1b6b/sydney.geojson')
gdf = gpd.GeoDataFrame(data, geometry=gpd.points_from_xy(data.lng, data.lat))


#Make sure the CRS matches between the two GeoDataFrames
gdf.crs = sydney_gdf.crs

# Function to plot cuisine density map
def show_cuisine_densitymap(cuisine, cmap='coolwarm'):
    # Filter restaurants that serve the specified cuisine
    filtered_gdf = gdf[gdf['cuisine'].apply(lambda x: cuisine in x)]

    # Perform spatial join
    joined_gdf = gpd.sjoin(sydney_gdf, filtered_gdf, how='left', predicate='contains')

    # Count the number of restaurants in each suburb
    joined_gdf['count'] = joined_gdf.groupby('subzone')['subzone'].transform('count')

    # Plot
    fig, ax = plt.subplots(1, figsize=(15, 10))
    joined_gdf.boundary.plot(ax=ax, linewidth=1)
    joined_gdf.plot(column='count', ax=ax, legend=True,
                    legend_kwds={'label': f"Number of {cuisine} Restaurants by Suburb"},
                    cmap=cmap, linewidth=0.8, edgecolor='0.8')

    plt.title(f"{cuisine} Cuisine Density by Suburb in Sydney")
    plt.show()

show_cuisine_densitymap(cuisine='Indian', cmap='coolwarm')

show_cuisine_densitymap(cuisine='Chinese', cmap='viridis')

"""### Interactive Ploting
In the case of visualizing restaurant types, non-interactive plotting limits the user's ability to explore the data deeply. For instance, static plots can show the distribution of restaurant types, but they don't allow users to interact with the data to uncover additional details like exact counts or to zoom into specific categories. Interactive plotting libraries like Plotly or Bokeh solve these limitations by enabling dynamic user interactions. Users can hover over bars to see exact counts, zoom in on specific categories, and even filter the data, providing a more comprehensive and engaging data exploration experience.
"""

import plotly.express as px

# Create an interactive histogram for 'Cost'
fig = px.histogram(data, x='cost', nbins=50, title='Distribution of Restaurant Cost',
                   labels={'cost': 'Cost (AUD)', 'count': 'Number of Restaurants'},
                   color_discrete_sequence=['purple'])

# Show the plot
fig.show()

# Create an interactive bar chart for 'Type'
fig = px.bar(df_types['Type'].value_counts().reset_index(),
             x='Type', y='index',
             labels={'Type': 'Count', 'index': 'Type'},
             title='Distribution of Restaurant Types',
             color_discrete_sequence=['blue'])

# Show the plot
fig.show()

"""## Part B – Predictive Modelling

## Feature Engineering
"""

# Check for missing values in the dataset
missing_values = data.isnull().sum()
missing_values

# Identify categorical and continuous variables
categorical_vars = data.select_dtypes(include=['object']).columns.tolist()
continuous_vars = data.select_dtypes(include=['int64', 'float64']).columns.tolist()

print('Categorical Variables:', categorical_vars)
print('Continuous Variables:', continuous_vars)

# Example for a DataFrame named df and a column named 'categories'

list_columns = []

for col in data.columns:
    not_null_values = data[col].dropna()
    if len(not_null_values) > 0:
        first_value = not_null_values.iloc[0]
        if isinstance(first_value, list):
            list_columns.append(col)

print("Columns containing lists:", list_columns)

for col in list_columns:
    data = data.explode(col)

data.head()

#Get the cleaned dataframe to csv for Tableu
data.to_csv('Zomato_final_new.csv', index=False)

# Add an identifier column
data['identifier'] = data['title']

# List of columns to keep for modeling
columns_to_keep = ['cost', 'cuisine', 'lat', 'lng', 'votes', 'groupon','rating_text']

# Create a new DataFrame with only the columns you want to keep
data_filtered = data[columns_to_keep]

# One-hot encode the filtered DataFrame
data_encoded = pd.get_dummies(data_filtered, drop_first=True)

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
# Define the features and the target
X = data_encoded  # All columns except 'rating_number' are features
y = data['rating_number']  # Target variable



# Split the data into training and testing sets
X_train, X_test, y_train, y_test, id_train, id_test = train_test_split(X, y, data['identifier'], test_size=0.2, random_state=0)

# Scale the features
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

"""### Building Linear Regression Models

1. **Linear Regression Model (model_regression_1)**
2. **Linear Regression with Gradient Descent (model_regression_2)**


"""

from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

# Build and evaluate the model
model_regression_1 = LinearRegression()
model_regression_1.fit(X_train_scaled, y_train)
y_pred_1 = model_regression_1.predict(X_test_scaled)

# Create a DataFrame to hold the test identifiers and corresponding predictions
result_df = pd.DataFrame({'Identifier': id_test, 'Predicted_Rating': y_pred_1})

# Calculate the mean squared error
mse_1 = mean_squared_error(y_test, y_pred_1)
print(f"Mean Squared Error for model_regression_1: {mse_1}")

from sklearn.model_selection import GridSearchCV

# Define hyperparameter grid
param_grid = {
    'alpha': [0.001, 0.01, 0.1],
    'max_iter': [1000, 2000],
    'tol': [1e-3, 1e-4]
}

# Initialize and run grid search
grid_search = GridSearchCV(SGDRegressor(random_state=0), param_grid, cv=5)
grid_search.fit(X_train_scaled, y_train)

# Get the best hyperparameters
best_params = grid_search.best_params_

from sklearn.linear_model import SGDRegressor

# Create a new instance of SGDRegressor with the best hyperparameters
best_alpha = best_params['alpha']
best_max_iter = best_params['max_iter']
best_tol = best_params['tol']

# Initialize the model
model_regression_2 = SGDRegressor(alpha=best_alpha, max_iter=best_max_iter, tol=best_tol, random_state=0)
# Fit the model to the training data
model_regression_2.fit(X_train_scaled, y_train)

# Make predictions on the test set
y_pred_2 = model_regression_2.predict(X_test_scaled)

# Calculate the mean squared error for the second model
mse_2 = mean_squared_error(y_test, y_pred_2)

# Create a DataFrame to hold the test identifiers and corresponding predictions for the second model
result_df_2 = pd.DataFrame({'Identifier': id_test, 'Predicted_Rating_2': y_pred_2})

print(f"Mean Squared Error for model_regression_1: {mse_1}")
print(f"Mean Squared Error for model_regression_2: {mse_2}")

# Merge the two result DataFrames on 'Identifier'
final_result_df = pd.merge(result_df, result_df_2, on='Identifier')

# Display the DataFrame with actual and predicted ratings
final_result_df['Actual_Rating'] = y_test.reset_index(drop=True)  # Resetting index for proper alignment
final_result_df.head()

"""## Classification"""

# Create a new DataFrame with only the columns you want to keep
data_filtered = data[columns_to_keep].copy()  # Make a copy to avoid the SettingWithCopyWarning

# Simplify the problem into binary classifications
binary_classification_map = {
    'Unknown':1,
    'Poor': 1,
    'Average': 1,
    'Good': 2,
    'Very Good': 2,
    'Excellent': 2
}

# Map 'rating_text' to 'rating_class'
data_filtered['rating_class'] = data_filtered['rating_text'].map(binary_classification_map)

# Select the columns of interest for comparison
comparison_df = data_filtered[['rating_text', 'rating_class']]

# Drop the 'rating_text' column
data_filtered.drop(columns=['rating_text'], inplace=True)

# One-hot encode the filtered DataFrame
data_encoded = pd.get_dummies(data_filtered, drop_first=True)

# Display the first few rows of the comparison DataFrame
print(comparison_df.head())

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix

# Define the features and the target for classification
X = data_encoded.drop(columns=['rating_class'])  # Use all encoded columns as features
y = data_encoded['rating_class']

# Split the data into training and testing sets (80% train, 20% test) for classification
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Build the logistic regression model
model_classification = LogisticRegression(max_iter=1000)
model_classification.fit(X_train, y_train)

# Predict on the test set
y_pred_class = model_classification.predict(X_test)

# Generate the confusion matrix
confusion_mt = confusion_matrix(y_test, y_pred_class)
confusion_mt

group_names = ['True Neg','False Pos','False Neg','True Pos']
group_counts = ["{0:0.0f}".format(value) for value in
confusion_mt.flatten()]
group_percentages = ["{0:.2%}".format(value) for value in
confusion_mt.flatten()/np.sum(confusion_mt)]
labels = [f"{v1}\n{v2}\n{v3}" for v1, v2, v3 in
zip(group_names,group_counts,group_percentages)]
labels = np.asarray(labels).reshape(2,2)
ax = sns.heatmap(confusion_mt, annot=labels, fmt='', cmap='Blues')
ax.set_title('Confusion Matrix with labels\n\n');
ax.set_xlabel('\nPredicted Values')
ax.set_ylabel('Actual Values ');
## Ticket labels - List must be in alphabetical order
ax.xaxis.set_ticklabels(['False','True'])
ax.yaxis.set_ticklabels(['False','True'])
## Display the visualization of the Confusion Matrix.
plt.show()

