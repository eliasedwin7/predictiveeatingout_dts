# Predictive Eating Out Analysis

This repository contains the source code, trained models, data, and visualizations for the Predictive Eating Out Analysis assignment.

## Table of Contents

- [Description](#description)
- [Installation](#installation)
- [Usage](#usage)


## Description

The purpose of this assignment is to perform data analysis and modeling on restaurant data to gain insights and predict various outcomes. The assignment was divided into multiple parts:

- **Part A:** Data wrangling and exploratory data analysis.
- **Part B:** Predictive modeling using regression and classification techniques.
- **Part C:** Deployment of the code on GitLab and creation of a Docker image.

## Installation

### Dependencies

Main dependencies include:
- pandas
- numpy
- sklearn
- geopandas
- matplotlib
- seaborn
- plotly

### Clone the Repository

To get a copy of this repository, run:

```bash
git clone https://gitlab.com/eliasedwin7/predictiveeatingout_dts.git
```

### Docker Setup

Pull the Docker image from Docker Hub, docker is designed to run jupyter notebook and pull the recent changes
from gitlab:

```bash
docker pull eliasedwin7/predictiveeatingout:latest
```

## Usage

### Running the Jupyter Notebook

To run the Jupyter Notebook inside the Docker container, execute the following command:

```bash
docker run -p 8888:8888 eliasedwin7/predictiveeatingout:latest
```

After executing the command, a link will appear in the terminal. Open this link in your browser to access the Jupyter Notebook.

## Tableau Dashboard

For quick insights and visualizations related to the Exploratory Data Analysis (EDA) questions, you can access my Tableau dashboard:
[View the Tableau Dashboard](https://public.tableau.com/app/profile/edwin.alais/viz/predictive_modelling_eating_out_problem/Dashboard1?publish=yes)


## Ensure that you don't have any jupyter session opened locally when running the docker , If opened locally docker will ask you access tokens ##