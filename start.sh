#!/bin/bash

echo "Starting start.sh script..."

# Navigate to app directory and pull the latest code
cd /app || { echo "Error navigating to /app directory"; exit 1; }
echo "Current directory: $(pwd)"
echo "Pulling latest code from repository..."
git pull || { echo "Error pulling from repository"; exit 1; }

# Start Jupyter Notebook
echo "Starting Jupyter Notebook..."
exec start-notebook.sh || { echo "Error starting Jupyter Notebook"; exit 1; }
