FROM jupyter/base-notebook:latest

USER root

WORKDIR /app

# Install Git
RUN apt-get update && \
    apt-get install -y git && \
    rm -rf /var/lib/apt/lists/*

RUN git config --global --add safe.directory /app

# Clone GitLab repository initially (it will be updated at runtime)
RUN git clone https://gitlab.com/eliasedwin7/predictiveeatingout_dts.git /app

# Install requirements
RUN pip install --requirement /app/requirements.txt

# Set execute permissions on start script
RUN chmod +x /app/start.sh

# Set working directory to the cloned repository folder
WORKDIR /app

# Run start script
CMD ["/app/start.sh"]
